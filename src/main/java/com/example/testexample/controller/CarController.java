package com.example.testexample.controller;

import com.example.testexample.domain.Car;
import com.example.testexample.repos.CarRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
@AllArgsConstructor
public class CarController {

  private CarRepository carRepository;

  @GetMapping
  public String main(Map<String, Object> cars) {
    Iterable<Car> carsList = carRepository.findAll();

    cars.put("carsList", carsList);

    return "main";
  }

  @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, value = "/create", produces = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public String add(Car car) {
    if (!car.getNumber().isEmpty()) {
      String number = car.getNumber();
      List<Car> errorList = carRepository.findByNumber(number);
      if (!errorList.isEmpty()) {
        return "error";
      }
    }
    carRepository.save(car);


    return "redirect:/";
  }

  @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, value = "/update", produces = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public String update(Car car) {
    if (car.getId() == null) {
      return "iderror";
    }
    if (!car.getNumber().isEmpty()) {
      String number = car.getNumber();
      List<Car> errorList = carRepository.findByNumber(number);
      if (errorList.size() > 1) {
        return "iderror";
      }
      carRepository.save(car);
    }
    return "redirect:/";
  }


  @PostMapping("filter")
  public String filter(@RequestParam String filter,
                       Map<String, Object> currentCar) {
    List<Car> carsList;

    if (filter != null && !filter.isEmpty()) {
      carsList = carRepository.findByNumber(filter);
    } else {
      carsList = (List<Car>) carRepository.findAll();
    }

    currentCar.put("carsList", carsList);

    return "main";
  }

  @GetMapping("/update/{id}")
  public String updateCar(Model model, @PathVariable("id") Integer id) {
    Car car = carRepository.findById(id);
    model.addAttribute("car", car);
    model.addAttribute("isUpdate", true);
    return "update";

  }

  @GetMapping("/delete/{id}")
  public String deleteCar(@PathVariable("id") Integer id) {
    carRepository.deleteCarById(id);
    return "deleted";
  }


}
