package com.example.testexample.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  private String mark;

  private String model;

  private String category;

  private String number;

  private String type;

  private String year;

  private String trailer;

}
